package pokemon_league;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class Pokemons {
    
        private String nombreP;
        private String natk1, natk2, natk3;
        private int atk1, atk2, atk3;
        private String type;
        private int hp, d_f;
        

    public Pokemons(String nombreP, String natk1, String natk2, String natk3,int atk1, int atk2, int atk3, String type, int hp, int d_f) {
        this.nombreP = nombreP;
        this.natk1 = natk1;
        this.natk2 = natk2;
        this.natk3 = natk3;
        this.atk1 = atk1;
        this.atk2 = atk2;
        this.atk3 = atk3;
        this.type = type;
        this.hp = hp;
        this.d_f = d_f;
    }
    
    public Pokemons(){
    }

    public String getNombreP() {
        return nombreP;
    }

    public void setNombreP(String nombreP) {
        this.nombreP = nombreP;
    }

    public String getNatk1() {
        return natk1;
    }

    public void setNatk1(String natk1) {
        this.natk1 = natk1;
    }

    public String getNatk2() {
        return natk2;
    }

    public void setNatk2(String natk2) {
        this.natk2 = natk2;
    }

    public String getNatk3() {
        return natk3;
    }

    public void setNatk3(String natk3) {
        this.natk3 = natk3;
    }

    public int getAtk1() {
        return atk1;
    }

    public void setAtk1(int atk1) {
        this.atk1 = atk1;
    }

    public int getAtk2() {
        return atk2;
    }

    public void setAtk2(int atk2) {
        this.atk2 = atk2;
    }

    public int getAtk3() {
        return atk3;
    }

    public void setAtk3(int atk3) {
        this.atk3 = atk3;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getD_f() {
        return d_f;
    }

    public void setD_f(int d_f) {
        this.d_f = d_f;
    }
    
    public ArrayList<Pokemons> puchamones(){
        ArrayList<Pokemons> pk =new ArrayList<Pokemons>();
        
        pk.add(new Pokemons("Pikachu", "Ataque rápido", "Impactrueno", "Cola de Hierro",10, 30, 20, "Eléctrico", 200,5));                        
        pk.add(new Pokemons("Lugia", "Golpe Aéreo", "Poder Pasado", "Meteorobola",10, 30, 20, "Viento/Psíquico", 200,5));                        
        pk.add(new Pokemons("Marshadow", "Poder Oculto", "Bola Sombra", "Demolición",10, 30, 20, "Siniestro/Lucha", 200,5));                        
        pk.add(new Pokemons("Blaziken", "Cuchillada", "Lanzallamas", "Patada Alta",10, 30, 20, "Fuego/Lucha", 200,5));                        
        pk.add(new Pokemons("Aggron", "Golpe de Roca", "Terremoto", "Cola Férrea",10, 30, 20, "Roca/Acero", 200,5));                        
        pk.add(new Pokemons("Blastoise", "Rayo Burbuja", "Hidrobomba", "Surf",10, 30, 20, "Agua", 200,5));                        
        pk.add(new Pokemons("Raikou", "Onda Voltio", "Trueno", "Chispa",10, 30, 20,"Eléctrico", 200,5));                        
        pk.add(new Pokemons("Dragonite", "Golpe Cuerpo", "Furia Dragón", "Puño Dinámico",10, 30, 20, "Dragón/Viento", 200,5));                        
        pk.add(new Pokemons("Lucario", "Pulso Umbrio", "Velocidad Extrema", "Garra Metal",10, 30, 20,"Lucha/Acero", 200,5));                        
        pk.add(new Pokemons("Deoxys", "Desarme", "Psicoataque", "Masa Cósmica",10, 30, 20,"", 200,5));
      
        return pk;
    }
                        
    public void daño1(){
        this.hp=hp-atk1;
    }
    
    public void daño2(){
        this.hp=hp-atk2;
    }
    
    public void daño3(){
        this.hp=hp-atk3;
    }
    
    public String toString(){
        return "Pokemon:"+ nombreP+"\n"
        +"Tipo: "+type+"\n"
        +"Nombre Ataque: "+natk1+"\n"
        +"Poder de Ataque: "+atk1+"\n"
        +"Nombre Ataque: "+natk2+"\n"
        +"Poder de Ataque: "+atk2+"\n"
        +"Nombre Ataque: "+natk3+"\n"
        +"Poder de Ataque: "+atk3+"\n"
        +"Salud: "+hp+"\n";
    }
    
    }
