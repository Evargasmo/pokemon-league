/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon_league;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Usuario
 */
public class Batallas {

    private Scanner leer;
    ArrayList<Entrenadores> entrenadores;
    ArrayList<Pokemons> puchamons;

    /**
     *
     */
    public Batallas() {
        leer = new Scanner(System.in);
        entrenadores = new ArrayList<Entrenadores>();
        puchamons = new ArrayList<Pokemons>();
        puchamons = puchamones();
    }

    /**
     *
     * @return
     */
    protected ArrayList<Entrenadores> crearJugadores() {
        int contadorEntrenadores = 0;
        boolean entrenadoresRequeridos = true;
        int id = 0;
        while (entrenadoresRequeridos) {
            System.out.println("Para agregar un entrenador digite 1 de lo contrario digite 0.\n \033[31mSE DEBE AGREGAR MINIMO 6 ENTRENADORES");
            int opcion = leer.nextInt();
            
            if (opcion == 0 && contadorEntrenadores < 6) {
                int restante = 6 - contadorEntrenadores;
                System.out.println("no se puede salir todavia! Se debe registrar " + restante + " Entrenadores mas como minimo");
            } else if (opcion == 0 && contadorEntrenadores >= 6) {
                entrenadoresRequeridos = false;
                break;
            }

            Pokemons[] pokemon = new Pokemons[3];
            String nombreE;
            

            System.out.println("\033[32mDigite el nombre del entrenador");
            nombreE = leer.next();
            for (int i = 1; i < puchamons.size(); i++) {
                Pokemons get = puchamons.get(i);

                System.out.println(" Numero: " + i + "  " + get.toString());
            }
            System.out.println("\033[32mSelecione 3 pokemones de la lista: ");
            for (int i = 0; i < pokemon.length; i++) {
                try {
                    int numero = leer.nextInt();
                    pokemon[i] = puchamons.get(numero);
                    puchamons.remove(numero);
                } catch (Exception e) {
                    System.out.println("\033[34meste numero de pokemon no esta disponible");
                }

            }
            entrenadores.add(new Entrenadores(nombreE,id, pokemon));
            contadorEntrenadores++;
            id++;
        }
        return entrenadores;
    }

   
    protected int crearBatalla(Entrenadores entrenador1, Entrenadores entrenador2) {
        Random rand = new Random(System.nanoTime());
        System.out.println("\033[34mEmpieza la batalla entre : " + entrenador1.getNombreE() + "\033[34m y su rival de toda la vida " + entrenador2.getNombreE() );
        boolean available = true;
        int victoria = 0;
        System.out.println("\033[32mTurno de " + entrenador1.getNombreE());
        entrenador1.trainerop();
        System.out.println("\033[32mTurno de " + entrenador2.getNombreE());
        entrenador2.trainerop();
        Pokemons pokemon1 = entrenador1.getPokemon();
        Pokemons pokemon2 = entrenador2.getPokemon();
        
        while(available == true){
            System.out.println("el pokemon: " + pokemon1.getNombreP() + "  esta atacando");
            pokemon2.daño1();
            System.out.println( pokemon2.getNombreP()+ " le a quedado una vida de: " + pokemon2.getHp());
            if (pokemon2.getHp() <= 0) {
                System.out.println("El ENTRENADOR " + entrenador1.getNombreE() + " Ha ganado la batalla.");
                pokemon1.setHp((pokemon1.getHp() * 50)/100);
                entrenador1.setWins(victoria++);
                available = false;
                return entrenador2.getId();
            } else {
                System.out.println("el pokemon: " + pokemon2.getNombreP() + " esta atacando");
                pokemon1.daño1();;
                System.out.println( pokemon1.getNombreP() + " le a quedado una vida de: " + pokemon1.getHp());
                if (pokemon1.getHp() <= 0) {
                    System.out.println("El Entrenador " + entrenador2.getNombreE() + " Ha ganado la batalla");
                    entrenador2.setWins(victoria++);
                    pokemon2.setHp((pokemon2.getHp() * 50)/100);
                    available = false;
                    return entrenador1.getId();
                }
            }
        }
        return 0;
    }

    /**
     *
     * @return
     */
    protected ArrayList<Pokemons> puchamones() {

        ArrayList<Pokemons> pk = new ArrayList<Pokemons>();

        pk.add(new Pokemons("Pikachu", "Ataque rápido", "Impactrueno", "Cola de Hierro",10, 30, 20, "Eléctrico", 200,5));                        
        pk.add(new Pokemons("Lugia", "Golpe Aéreo", "Poder Pasado", "Meteorobola",10, 30, 20, "Viento/Psíquico", 200,5));                        
        pk.add(new Pokemons("Marshadow", "Poder Oculto", "Bola Sombra", "Demolición",10, 30, 20, "Siniestro/Lucha", 200,5));                        
        pk.add(new Pokemons("Blaziken", "Cuchillada", "Lanzallamas", "Patada Alta",10, 30, 20, "Fuego/Lucha", 200,5));                        
        pk.add(new Pokemons("Aggron", "Golpe de Roca", "Terremoto", "Cola Férrea",10, 30, 20, "Roca/Acero", 200,5));                        
        pk.add(new Pokemons("Blastoise", "Rayo Burbuja", "Hidrobomba", "Surf",10, 30, 20, "Agua", 200,5));                        
        pk.add(new Pokemons("Raikou", "Onda Voltio", "Trueno", "Chispa",10, 30, 20,"Eléctrico", 200,5));                        
        pk.add(new Pokemons("Dragonite", "Golpe Cuerpo", "Furia Dragón", "Puño Dinámico",10, 30, 20, "Dragón/Viento", 200,5));                        
        pk.add(new Pokemons("Lucario", "Pulso Umbrio", "Velocidad Extrema", "Garra Metal",10, 30, 20,"Lucha/Acero", 200,5));                        
        pk.add(new Pokemons("Deoxys", "Desarme", "Psicoataque", "Masa Cósmica",10, 30, 20,"", 200,5));

        return pk;
    }

}