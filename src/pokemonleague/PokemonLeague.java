/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemonleague;

import java.util.Scanner;

/**
 *
 * @author Amoshito
 */
public class PokemonLeague {
    
    public static void main(String[] args) {
        
        Scanner leer = new Scanner(System.in);
        System.out.println("\033[31mBienvenido al Torneo Pokemon\n");
        System.out.println("\033[32mPor favor ingresa la cantidad de entrenadores que participarán en el torneo");
        int nEntrenadores = leer.nextInt();
        String [] entrenadores = new String[nEntrenadores];
        
        Pokemons ps = new Pokemons();
        
        System.out.println("\033[31mAhora pasaremos al registro de los participantes del torneo\n");
        
        for(int i = 0; i < nEntrenadores; i++){
            System.out.println("\033[34mIngresa el nombre del ("+(i+1)+") Entrenador");
            String cant = leer.next();
            entrenadores[i] = cant;
            ps.elegirPokemon();
        }
        
        for(int i = 0; i < nEntrenadores; i++){
            System.out.println("\033[34mEntrenador # "+(i+1)+" :"+entrenadores[i]);
        }
        
        
        System.out.println("\033[36m\n Para registar \n");
        
         for (int i = 0; i < nEntrenadores; i++) {
              System.out.println("\033[35mEntrenador: " + entrenadores[i] + "\033[34m/ su pokemon es: " + ps.nombreP + "\033[33m/ Vida: " + ps.getSalud() + "\033[32m/ Ataque: " + ps.getAtk()+ "\033[31m/ Tipo: " + ps.getTipo());
              
        
    
    }
    }   
}
